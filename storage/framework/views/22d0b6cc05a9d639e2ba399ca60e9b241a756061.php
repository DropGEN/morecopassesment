<header>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">MoreCop <>
            <?php echo e(Auth::user()->name); ?>

        </a>

        <input class="form-control form-control-dark w-70" type="text" placeholder="Search" aria-label="Search">
        <ul class="navbar-nav px-md-2">

            <li class="nav-item text-nowrap">
                <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <?php echo e(__('Sign out')); ?>

                </a>
                <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                    <?php echo csrf_field(); ?>
                </form>
            </li>


        </ul>
    </nav>
</header>
