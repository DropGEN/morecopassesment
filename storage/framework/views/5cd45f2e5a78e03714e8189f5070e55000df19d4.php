<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<meta name="description" content="">

<meta name="author" content="">

<!-- Favicon -->
<link rel="shortcut icon" href="<?php echo e(asset('images/favicon.jpg')); ?>">

<!-- CSFR token for ajax call -->

<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

<link rel="canonical" href="https://getbootstrap.com/docs/4.1/examples/dashboard/">

<title>Assesment</title>

<!-- Bootstrap core CSS -->

<link href="/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="/css/dashboard.css" rel="stylesheet">







