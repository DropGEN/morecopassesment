<?php $__env->startSection('content'); ?>
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">

        <div class="container">
            <form name="leave-application-form" class="form-horizontal" method="POST" action="/product">

                <?php echo e(csrf_field()); ?>

                <div class="box-body">
                    <?php if(count($errors) > 0): ?>
                        <div class="alert alert-danger alert-dismissible fade in">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-ban"></i> Invalid Input Data!</h4>
                            <ul>
                                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><?php echo e($error); ?></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                    <h2> Add Products </h2>

                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>

                    <div class="form-group">
                        <label for="price">Price:</label>
                        <input type="number" class="form-control" id="price" name="price">
                    </div>

                    <div class="form-group">
                        <label for="type">Sku:</label>
                        <input type="text" class="form-control" id="sku" name="sku">
                    </div>
                    <div class="form-group">
                        <label for="type">Description:</label>
                        <textarea class="form-control rounded-0" id="exampleFormControlTextarea2" name="description"
                                  rows="3" placeholder="Description"></textarea>
                    </div>

                    <div class="text-center">
                        <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png"
                             class="avatar img-circle img-thumbnail" alt="avatar">
                        <h6>Upload an Image </h6>
                        <input type="file" id="image" name="image"
                               class="text-center center-block file-upload"
                               data-allowed-file-extensions='["jpg", "jpeg", "png"]'>
                    </div>

                    <button class="btn btn-primary pull-right">Submit</button>
            </form>
        </div>

        <br>
        <br>
        <!-- Default form contact -->
        <h2>Products </h2>
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Skue</th>
                    <th>Description</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                <?php $__currentLoopData = $Product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>

                        <td><?php echo e($list->id); ?></td>
                        <td><?php echo e(!empty($list->name) ? $list->name : ''); ?></td>
                        <td><?php echo e(!empty($list->description) ? $list->description : ''); ?></td>
                        <td><?php echo e(!empty($list->sku) ? $list->sku : ''); ?></td>
                        <td><?php echo e(!empty($list->price) ?  'R' .number_format($list->price, 2): ''); ?></td>
                        <td><a href="<?php echo e(url('/view/' . $list->id .'/bid')); ?>" class="btn btn-xs btn-info pull-left">view
                                bid </a>
                        <td>
                        <td><a href="<?php echo e(url('/view/' . $list->id .'/bid')); ?>"
                               class="btn btn-xs btn-outline-danger pull-left">Edit </a>
                        <td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </tbody>
            </table>
            <div align="center">
                <?php echo e($Product->links()); ?>

            </div>
        </div>
    </main>

    <br>
    <br>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layout.adminlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>